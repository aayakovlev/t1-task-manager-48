package ru.t1.aayakovlev.tm.comparator;

import org.jetbrains.annotations.Nullable;
import ru.t1.aayakovlev.tm.model.HasCreated;

import java.util.Comparator;

public enum CreatedComparator implements Comparator<HasCreated> {

    INSTANCE;

    @Override
    public int compare(@Nullable final HasCreated o1, @Nullable final HasCreated o2) {
        if (o1 == null || o2 == null) return 0;
        return o1.getCreated().compareTo(o2.getCreated());
    }

}
