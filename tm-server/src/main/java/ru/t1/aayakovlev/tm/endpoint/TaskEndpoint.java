package ru.t1.aayakovlev.tm.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.aayakovlev.tm.dto.request.*;
import ru.t1.aayakovlev.tm.dto.response.*;
import ru.t1.aayakovlev.tm.exception.AbstractException;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public interface TaskEndpoint extends BaseEndpoint {

    @NotNull
    String NAME = "TaskEndpointImpl";

    @NotNull
    String PART = NAME + "Service";

    @SneakyThrows
    @WebMethod(exclude = true)
    static TaskEndpoint newInstance() {
        return newInstance(HOST, PORT);
    }

    @SneakyThrows
    @WebMethod(exclude = true)
    static TaskEndpoint newInstance(@NotNull final ConnectionProvider connectionProvider) {
        return BaseEndpoint.newInstance(connectionProvider, NAME, SPACE, PART, TaskEndpoint.class);
    }

    @SneakyThrows
    @WebMethod(exclude = true)
    static TaskEndpoint newInstance(@NotNull final String host, @NotNull final String port) {
        return BaseEndpoint.newInstance(host, port, NAME, SPACE, PART, TaskEndpoint.class);
    }

    @NotNull
    @WebMethod
    TaskBindToProjectResponse bindTaskToProject(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskBindToProjectRequest request
    ) throws AbstractException;

    @NotNull
    @WebMethod
    TaskChangeStatusByIdResponse changeTaskStatusById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskChangeStatusByIdRequest request
    ) throws AbstractException;

    @NotNull
    @WebMethod
    TaskClearResponse clearTask(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskClearRequest request
    ) throws AbstractException;

    @NotNull
    @WebMethod
    TaskCompleteByIdResponse completeTaskById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskCompleteByIdRequest request
    ) throws AbstractException;

    @NotNull
    @WebMethod
    TaskCreateResponse createTask(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskCreateRequest request
    ) throws AbstractException;

    @NotNull
    @WebMethod
    TaskRemoveByIdResponse removeTaskById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskRemoveByIdRequest request
    ) throws AbstractException;

    @NotNull
    @WebMethod
    TaskShowAllResponse showAllTasks(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskShowAllRequest request
    ) throws AbstractException;

    @NotNull
    @WebMethod
    TaskShowByIdResponse showTaskById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskShowByIdRequest request
    ) throws AbstractException;

    @NotNull
    @WebMethod
    TaskShowByProjectIdResponse showTasksByProjectId(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskShowByProjectIdRequest request
    ) throws AbstractException;

    @NotNull
    @WebMethod
    TaskStartByIdResponse startTaskById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskStartByIdRequest request
    ) throws AbstractException;

    @NotNull
    @WebMethod
    TaskUnbindFromProjectResponse unbindTaskFromProject(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskUnbindFromProjectRequest request
    ) throws AbstractException;

    @NotNull
    @WebMethod
    TaskUpdateByIdResponse updateTaskById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskUpdateByIdRequest request
    ) throws AbstractException;

}
