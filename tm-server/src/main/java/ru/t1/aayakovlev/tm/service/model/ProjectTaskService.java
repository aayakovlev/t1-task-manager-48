package ru.t1.aayakovlev.tm.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.aayakovlev.tm.exception.AbstractException;
import ru.t1.aayakovlev.tm.model.Task;

public interface ProjectTaskService {

    @NotNull
    Task bindTaskToProject(
            @Nullable final String userId,
            @Nullable final String projectId,
            @Nullable final String taskId
    ) throws AbstractException;

    @NotNull
    Task unbindTaskFromProject(
            @Nullable final String userId,
            @Nullable final String projectId,
            @Nullable final String taskId
    ) throws AbstractException;

    void removeProjectById(
            @Nullable final String userId,
            @Nullable final String projectId
    ) throws AbstractException;

    void clearProjects(
            @Nullable final String userId
    ) throws AbstractException;

}
