package ru.t1.aayakovlev.tm.service.dto.impl;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.aayakovlev.tm.dto.model.ProjectDTO;
import ru.t1.aayakovlev.tm.dto.model.TaskDTO;
import ru.t1.aayakovlev.tm.exception.AbstractException;
import ru.t1.aayakovlev.tm.exception.auth.AuthenticationException;
import ru.t1.aayakovlev.tm.exception.entity.ProjectNotFoundException;
import ru.t1.aayakovlev.tm.exception.entity.TaskNotFoundException;
import ru.t1.aayakovlev.tm.exception.field.IdEmptyException;
import ru.t1.aayakovlev.tm.exception.field.ProjectIdEmptyException;
import ru.t1.aayakovlev.tm.exception.field.TaskIdEmptyException;
import ru.t1.aayakovlev.tm.repository.dto.ProjectDTORepository;
import ru.t1.aayakovlev.tm.repository.dto.TaskDTORepository;
import ru.t1.aayakovlev.tm.repository.dto.impl.ProjectDTORepositoryImpl;
import ru.t1.aayakovlev.tm.repository.dto.impl.TaskDTORepositoryImpl;
import ru.t1.aayakovlev.tm.service.ConnectionService;
import ru.t1.aayakovlev.tm.service.dto.ProjectTaskDTOService;

import javax.persistence.EntityManager;
import java.util.List;

public final class ProjectTaskDTOServiceImpl implements ProjectTaskDTOService {

    @NotNull
    private final ConnectionService connectionService;

    public ProjectTaskDTOServiceImpl(@NotNull final ConnectionService connectionService) {
        this.connectionService = connectionService;
    }

    @NotNull
    private EntityManager getEntityManager() {
        return connectionService.getEntityManager();
    }

    @NotNull
    private ProjectDTORepository getProjectRepository(@NotNull final EntityManager entityManager) {
        return new ProjectDTORepositoryImpl(entityManager);
    }

    @NotNull
    private TaskDTORepository getTaskRepository(@NotNull final EntityManager entityManager) {
        return new TaskDTORepositoryImpl(entityManager);
    }

    @NotNull
    @Override
    public TaskDTO bindTaskToProject(
            @Nullable final String userId,
            @Nullable final String projectId,
            @Nullable final String taskId
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new AuthenticationException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (taskId == null || taskId.isEmpty()) throw new TaskIdEmptyException();
        @Nullable TaskDTO resultTask;
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final ProjectDTORepository projectRepository = getProjectRepository(entityManager);
            if (projectRepository.findById(userId, projectId) == null) throw new ProjectNotFoundException();
            @NotNull final TaskDTORepository taskRepository = getTaskRepository(entityManager);
            resultTask = taskRepository.findById(userId, taskId);
            if (resultTask == null) throw new TaskNotFoundException();
            resultTask.setProjectId(projectId);
            resultTask.setUserId(userId);
            entityManager.getTransaction().begin();
            taskRepository.update(resultTask);
            entityManager.getTransaction().commit();
            resultTask = taskRepository.findById(taskId);
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return resultTask;
    }

    @NotNull
    @Override
    public TaskDTO unbindTaskFromProject(
            @Nullable final String userId,
            @Nullable final String projectId,
            @Nullable final String taskId
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new AuthenticationException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (taskId == null || taskId.isEmpty()) throw new TaskIdEmptyException();
        @Nullable TaskDTO resultTask;
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final ProjectDTORepository projectRepository = getProjectRepository(entityManager);
            if (projectRepository.findById(userId, projectId) == null) throw new ProjectNotFoundException();
            @NotNull final TaskDTORepository taskRepository = getTaskRepository(entityManager);
            resultTask = taskRepository.findById(userId, taskId);
            if (resultTask == null) throw new TaskNotFoundException();
            resultTask.setProjectId(null);
            resultTask.setUserId(userId);
            entityManager.getTransaction().begin();
            taskRepository.update(resultTask);
            entityManager.getTransaction().commit();
            resultTask = taskRepository.findById(taskId);
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return resultTask;
    }

    @Override
    public void removeProjectById(
            @Nullable final String userId,
            @Nullable final String projectId
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new AuthenticationException();
        if (projectId == null || projectId.isEmpty()) throw new IdEmptyException();

        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final ProjectDTORepository projectRepository = getProjectRepository(entityManager);
            @NotNull final TaskDTORepository taskRepository = getTaskRepository(entityManager);
            @Nullable ProjectDTO project = projectRepository.findById(userId, projectId);
            if (project == null) throw new ProjectNotFoundException();
            entityManager.getTransaction().begin();
            taskRepository.removeAllByProjectId(userId, projectId);
            projectRepository.removeById(userId, projectId);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void clearProjects(@Nullable final String userId) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new AuthenticationException();
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final ProjectDTORepository projectRepository = getProjectRepository(entityManager);
            @NotNull final TaskDTORepository taskRepository = getTaskRepository(entityManager);
            @NotNull final List<ProjectDTO> projects = projectRepository.findAll(userId);
            entityManager.getTransaction().begin();
            for (@NotNull final ProjectDTO project : projects) {
                taskRepository.removeAllByProjectId(userId, project.getId());
            }
            projectRepository.clear(userId);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

}
