package ru.t1.aayakovlev.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import ru.t1.aayakovlev.tm.model.Project;

public interface ProjectRepository extends ExtendedRepository<Project> {

    @NotNull
    Project create(
            @NotNull final String userId,
            @NotNull final String name
    );

    @NotNull
    Project create(
            @NotNull final String userId,
            @NotNull final String name,
            @NotNull final String description
    );

}
