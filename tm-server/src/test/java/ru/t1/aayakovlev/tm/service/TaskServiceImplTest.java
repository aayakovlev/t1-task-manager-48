package ru.t1.aayakovlev.tm.service;

import liquibase.Liquibase;
import liquibase.exception.LiquibaseException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.t1.aayakovlev.tm.dto.model.TaskDTO;
import ru.t1.aayakovlev.tm.enumerated.Sort;
import ru.t1.aayakovlev.tm.exception.AbstractException;
import ru.t1.aayakovlev.tm.exception.entity.EntityNotFoundException;
import ru.t1.aayakovlev.tm.exception.entity.TaskNotFoundException;
import ru.t1.aayakovlev.tm.marker.UnitCategory;
import ru.t1.aayakovlev.tm.migration.AbstractSchemeTest;
import ru.t1.aayakovlev.tm.service.dto.ProjectDTOService;
import ru.t1.aayakovlev.tm.service.dto.TaskDTOService;
import ru.t1.aayakovlev.tm.service.dto.UserDTOService;
import ru.t1.aayakovlev.tm.service.dto.impl.ProjectDTOServiceImpl;
import ru.t1.aayakovlev.tm.service.dto.impl.TaskDTOServiceImpl;
import ru.t1.aayakovlev.tm.service.dto.impl.UserDTOServiceImpl;
import ru.t1.aayakovlev.tm.service.impl.ConnectionServiceImpl;
import ru.t1.aayakovlev.tm.service.impl.PropertyServiceImpl;

import java.util.List;

import static ru.t1.aayakovlev.tm.constant.ApplicationConstant.LIQUIBASE_CHANGELOG_FILENAME;
import static ru.t1.aayakovlev.tm.constant.ProjectTestConstant.*;
import static ru.t1.aayakovlev.tm.constant.TaskTestConstant.*;
import static ru.t1.aayakovlev.tm.constant.TaskTestConstant.DESCRIPTION;
import static ru.t1.aayakovlev.tm.constant.TaskTestConstant.NAME;
import static ru.t1.aayakovlev.tm.constant.UserTestConstant.*;

@Category(UnitCategory.class)
public final class TaskServiceImplTest extends AbstractSchemeTest {

    @NotNull
    private static PropertyService propertyService;

    @NotNull
    private static ConnectionService connectionService;

    @NotNull
    private static TaskDTOService service;

    @NotNull
    private static UserDTOService userService;

    @NotNull
    private static ProjectDTOService projectService;

    @BeforeClass
    public static void initConnectionService() throws LiquibaseException {
        @NotNull final Liquibase liquibase = liquibase(LIQUIBASE_CHANGELOG_FILENAME);
        liquibase.dropAll();
        liquibase.update("scheme");

        propertyService = new PropertyServiceImpl();
        connectionService = new ConnectionServiceImpl(propertyService);
        service = new TaskDTOServiceImpl(connectionService);
        projectService = new ProjectDTOServiceImpl(connectionService);
        userService = new UserDTOServiceImpl(connectionService, propertyService);
    }

    @AfterClass
    public static void destroyConnection() {
        connectionService.close();
    }

    @Before
    public void initData() throws AbstractException {
        userService.save(COMMON_USER_ONE);
        userService.save(ADMIN_USER_ONE);
        projectService.save(PROJECT_USER_ONE);
        projectService.save(PROJECT_USER_TWO);
        projectService.save(PROJECT_ADMIN_ONE);
        projectService.save(PROJECT_ADMIN_TWO);
        service.save(TASK_USER_ONE);
        service.save(TASK_USER_TWO);
    }

    @After
    public void afterClear() throws AbstractException {
        service.clear();
        projectService.clear();
        userService.clear();
    }

    @Test
    public void When_FindByIdExistsProject_Expect_ReturnProject() throws AbstractException {
        @Nullable final TaskDTO task = service.findById(COMMON_USER_ONE.getId(), TASK_USER_ONE.getId());
        Assert.assertNotNull(task);
        Assert.assertEquals(TASK_USER_ONE.getDescription(), task.getDescription());
        Assert.assertEquals(TASK_USER_ONE.getName(), task.getName());
        Assert.assertEquals(TASK_USER_ONE.getStatus(), task.getStatus());
        Assert.assertEquals(TASK_USER_ONE.getUserId(), task.getUserId());
        Assert.assertEquals(TASK_USER_ONE.getCreated(), task.getCreated());
    }

    @Test
    public void When_FindByIdExistsProject_Expect_ThrowsEntityNotFoundException() {
        Assert.assertThrows(EntityNotFoundException.class,
                () -> service.findById(USER_ID_NOT_EXISTED, TASK_ID_NOT_EXISTED)
        );

    }

    @Test
    public void When_SaveNotNullProject_Expect_ReturnProject() throws AbstractException {
        @NotNull final TaskDTO savedTask = service.save(TASK_ADMIN_ONE);
        Assert.assertNotNull(savedTask);
        Assert.assertEquals(TASK_ADMIN_ONE, savedTask);
        @Nullable final TaskDTO task = service.findById(ADMIN_USER_ONE.getId(), TASK_ADMIN_ONE.getId());
        Assert.assertNotNull(task);
        Assert.assertEquals(TASK_ADMIN_ONE.getId(), task.getId());
        Assert.assertEquals(TASK_ADMIN_ONE.getName(), task.getName());
        Assert.assertEquals(TASK_ADMIN_ONE.getProjectId(), task.getProjectId());
        Assert.assertEquals(TASK_ADMIN_ONE.getDescription(), task.getDescription());
        Assert.assertEquals(TASK_ADMIN_ONE.getCreated(), task.getCreated());
        Assert.assertEquals(TASK_ADMIN_ONE.getStatus(), task.getStatus());
        Assert.assertEquals(TASK_ADMIN_ONE.getUserId(), task.getUserId());
    }

    @Test
    public void When_CountCommonUserProjects_Expect_ReturnTwo() throws AbstractException {
        final int count = service.count(COMMON_USER_ONE.getId());
        Assert.assertEquals(2, count);
    }

    @Test
    public void When_FindAllByProjectId_Expect_ReturnTasks() throws AbstractException {
        Assert.assertEquals(1, service.findAllByProjectId(COMMON_USER_ONE.getId(), PROJECT_USER_ONE.getId()).size());
    }

    @Test
    public void When_RemoveExistedProject_Expect_ReturnProject() throws AbstractException {
        Assert.assertNotNull(service.save(TASK_ADMIN_TWO));
        service.removeById(ADMIN_USER_ONE.getId(), TASK_ADMIN_TWO.getId());
    }

    @Test
    public void When_RemoveNotProject_Expect_ThrowsEntityNotFoundException() {
        Assert.assertThrows(EntityNotFoundException.class,
                () -> service.removeById(ADMIN_USER_ONE.getId(), TASK_NOT_EXISTED.getId())
        );
    }

    @Test
    public void When_RemoveAll_Expect_ZeroCountProjects() throws AbstractException {
        service.save(TASK_ADMIN_ONE);
        service.save(TASK_ADMIN_TWO);
        service.clear(ADMIN_USER_ONE.getId());
        Assert.assertEquals(0, service.count(ADMIN_USER_ONE.getId()));
    }

    @Test
    public void When_RemoveByIdExistedProject_Expect_Project() throws AbstractException {
        Assert.assertNotNull(service.save(TASK_ADMIN_TWO));
        service.removeById(ADMIN_USER_ONE.getId(), TASK_ADMIN_TWO.getId());
    }

    @Test
    public void When_RemoveByIdNotExistedProject_Expect_ThrowsEntityNotFoundException() {
        Assert.assertThrows(
                EntityNotFoundException.class,
                () -> service.removeById(ADMIN_USER_ONE.getId(), TASK_NOT_EXISTED.getId())
        );
    }

    @Test
    public void When_CreateNameProject_Expect_ExistedProject() throws AbstractException {
        @Nullable final TaskDTO task = service.create(ADMIN_USER_ONE.getId(), NAME);
        Assert.assertNotNull(task);
        Assert.assertEquals(NAME, task.getName());
    }

    @Test
    public void When_CreateNameDescriptionProject_Expect_ExistedProject() throws AbstractException {
        @Nullable final TaskDTO task = service.create(ADMIN_USER_ONE.getId(), NAME, DESCRIPTION);
        Assert.assertNotNull(task);
        Assert.assertEquals(NAME, task.getName());
        Assert.assertEquals(DESCRIPTION, task.getDescription());
    }

    @Test
    public void When_UpdateByIdProject_Expect_UpdatedProject() throws AbstractException {
        service.save(TASK_ADMIN_TWO);
        @Nullable TaskDTO task = service.update(
                TASK_ADMIN_TWO.getUserId(), TASK_ADMIN_TWO.getId(), NAME, DESCRIPTION
        );
        Assert.assertEquals(NAME, task.getName());
        Assert.assertEquals(DESCRIPTION, task.getDescription());
    }

}
