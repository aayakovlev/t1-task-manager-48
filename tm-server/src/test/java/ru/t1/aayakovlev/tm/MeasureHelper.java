package ru.t1.aayakovlev.tm;

import org.jetbrains.annotations.NotNull;

import javax.persistence.EntityManager;

public final class MeasureHelper {

    private MeasureHelper() {}

    @FunctionalInterface
    public interface MeasureCode {
        void run();
    }

    public static void measure(@NotNull final EntityManager entityManager, @NotNull final MeasureCode func) {
        try {
            entityManager.getTransaction().begin();
            func.run();
            entityManager.getTransaction().commit();
        } catch (@NotNull final Throwable t) {
            t.printStackTrace();
        }
    }

}
