package ru.t1.aayakovlev.tm.migration;

import liquibase.Liquibase;
import liquibase.database.Database;
import liquibase.database.DatabaseFactory;
import liquibase.database.jvm.JdbcConnection;
import liquibase.exception.DatabaseException;
import liquibase.resource.ClassLoaderResourceAccessor;
import org.jetbrains.annotations.NotNull;
import org.junit.AfterClass;
import org.junit.BeforeClass;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public abstract class AbstractSchemeTest {

    @NotNull
    private static final ClassLoaderResourceAccessor ACCESSOR = new ClassLoaderResourceAccessor();

    @NotNull
    private static Database DATABASE;

    @BeforeClass
    public static void init() throws IOException, DatabaseException, SQLException {
        @NotNull final Properties properties = new Properties();
        @NotNull final InputStream inputStream = ClassLoader.getSystemResourceAsStream("liquibase.properties");
        properties.load(inputStream);
        @NotNull final Connection connection = getConnection(properties);
        @NotNull final JdbcConnection jdbcConnection = new JdbcConnection(connection);
        DATABASE = DatabaseFactory.getInstance().findCorrectDatabaseImplementation(jdbcConnection);
    }

    @AfterClass
    public static void cleanup() throws DatabaseException {
        DATABASE.close();
    }

    @NotNull
    protected static Liquibase liquibase(@NotNull final String filename) {
        return new Liquibase(filename, ACCESSOR, DATABASE);
    }

    @NotNull
    private static Connection getConnection(@NotNull final Properties properties) throws SQLException {
        return DriverManager.getConnection(
                properties.getProperty("url"),
                properties.getProperty("username"),
                properties.getProperty("password")
        );
    }

}
